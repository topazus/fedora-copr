%if 0%{?fedora} >= 33
%global build_terminfo 0
%else
%global build_terminfo 1
%endif

Name:          alacritty-terminal
Version:       0.8.0
Release:       1%{?dist}
Summary:       A cross-platform, GPU enhanced terminal emulator

# Upstream license specification: Apache-2.0
License:       ASL 2.0
URL:           https://github.com/alacritty/alacritty
Source0:       https://github.com/alacritty/alacritty/archive/v%{version}-rc3.tar.gz

%if !%{build_terminfo}
BuildRequires: ncurses >= 6.2
%endif

BuildRequires: rust >= 1.43.0
BuildRequires: cargo
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: python3
BuildRequires: freetype-devel
BuildRequires: fontconfig-devel
BuildRequires: libxcb-devel
BuildRequires: desktop-file-utils
BuildRequires: ncurses

Requires: freetype
Requires: fontconfig
Requires: libxcb

%description
A cross-platform, GPU enhanced terminal emulator.

%prep
%autosetup -n alacritty-%{version}-rc3 -p1
#%setup -q -n alacritty-%{version}

%build
cargo build --release

%install
install -p -D -m755 target/release/alacritty         %{buildroot}%{_bindir}/alacritty
install -p -D -m644 extra/linux/Alacritty.desktop    %{buildroot}%{_datadir}/applications/Alacritty.desktop
install -p -D -m644 extra/logo/alacritty-term.svg    %{buildroot}%{_datadir}/pixmaps/Alacritty.svg
install -p -D -m644 alacritty.yml                    %{buildroot}%{_datadir}/alacritty/alacritty.yml
install -p -D -m644 extra/completions/alacritty.bash %{buildroot}%{_datadir}/bash-completion/completions/alacritty
install -p -D -m644 extra/completions/_alacritty     %{buildroot}%{_datadir}/zsh/site-functions/_alacritty
install -p -D -m644 extra/completions/alacritty.fish %{buildroot}%{_datadir}/fish/vendor_completions.d/alacritty.fish
install -p -D -m644 extra/alacritty.man              %{buildroot}%{_mandir}/man1/alacritty.1
%if %{build_terminfo}
tic -xe alacritty,alacritty-direct extra/alacritty.info -o %{buildroot}%{_datadir}/terminfo
%endif

%files
%license LICENSE-APACHE
%doc README.md
%{_bindir}/alacritty
%{_datadir}/applications/Alacritty.desktop
%{_datadir}/pixmaps/Alacritty.svg
%if %{build_terminfo}
%{_datadir}/terminfo/a/alacritty*
%endif

%dir %{_datadir}/alacritty/
%{_datadir}/alacritty/alacritty.yml
%{_datadir}/bash-completion/completions/alacritty
%{_datadir}/zsh/site-functions/_alacritty
%{_datadir}/fish/vendor_completions.d/alacritty.fish

%{_mandir}/man1/alacritty.1*

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/Alacritty.desktop

%changelog
* Sun May 9 2021 Felix Wang <topazus@outlook.com> - 0.8.0-rc3
- build update to 0.8.0-rc3
