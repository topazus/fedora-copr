%global debug_package %{nil}
%global rustflags '-Clink-arg=-Wl,-z,relro,-z,now'
%define vtag 20210502-154244-3f7122cb

Name:          wezterm
Version:       2021.05.02
Release:       1%{?dist}
Summary:       A GPU-accelerated cross-platform terminal emulator and multiplexer

License:       MIT
URL:           https://github.com/wez/wezterm
Source0:       https://github.com/wez/wezterm/releases/download/%{vtag}/wezterm-%{vtag}-src.tar.gz

BuildRequires: rust >= 1.51.0
BuildRequires: cargo
BuildRequires: perl-FindBin
BuildRequires: perl-File-Compare
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: fontconfig-devel
BuildRequires: openssl-devel
BuildRequires: perl-interpreter
BuildRequires: python3
BuildRequires: libxcb-devel
BuildRequires: libxkbcommon-devel
BuildRequires: libxkbcommon-x11-devel
BuildRequires: wayland-devel
BuildRequires: mesa-libEGL-devel
BuildRequires: xcb-util-keysyms-devel
BuildRequires: xcb-util-image-devel
BuildRequires: xcb-util-wm-devel
BuildRequires: rpm-build
BuildRequires: redhat-lsb-core
BuildRequires: desktop-file-utils

Requires: openssl

%description
A GPU-accelerated cross-platform terminal emulator and multiplexer.

%prep
%autosetup -n wezterm-%{vtag} -p1

%build
#cargo build --release
RUSTFLAGS=%{rustflags} cargo build --release

%install
install -p -D -m755 target/release/wezterm              %{buildroot}%{_bindir}/wezterm
install -p -D -m755 target/release/wezterm-gui          %{buildroot}%{_bindir}/wezterm-gui
install -p -D -m755 target/release/wezterm-mux-server   %{buildroot}%{_bindir}/wezterm-mux-server
install -p -D -m755 target/release/strip-ansi-escapes   %{buildroot}%{_bindir}/strip-ansi-escapes
install -p -D -m644 assets/wezterm.desktop              %{buildroot}%{_datadir}/applications/wezterm.desktop
install -p -D -m644 assets/icon/terminal.png            %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/wezterm.png
install -p -D -m644 assets/shell-integration/wezterm.sh %{buildroot}%{_sysconfdir}/profile.d/wezterm.sh
install -p -D -m644 assets/wezterm.appdata.xml          %{buildroot}%{_metainfodir}/wezterm.appdata.xml

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/wezterm.desktop

%files
%license LICENSE.md
%doc README.md
%{_bindir}/wezterm
%{_bindir}/wezterm-gui
%{_bindir}/wezterm-mux-server
%{_bindir}/strip-ansi-escapes
%{_datadir}/applications/wezterm.desktop
%{_datadir}/icons/hicolor/128x128/apps/wezterm.png
%{_sysconfdir}/profile.d/wezterm.sh
%{_metainfodir}/wezterm.appdata.xml

%changelog
* Sun May 9 2021 Felix Wang <topazus@outlook.com> - 2021.05.02
- build update to 2021.05.02
