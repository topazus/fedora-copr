%global debug_package %{nil}

Name:           rust-zoxide
Version:        0.7.0
Release:        1%{?dist}
Summary:        Faster way to navigate your filesystem

License:        MIT
URL:            https://github.com/ajeetdsouza/zoxide
Source:         %{url}/archive/v%{version}.tar.gz

BuildRequires:  cargo >= 1.40
BuildRequires:  rust >= 1.40
BuildRequires:  rust-packaging

%global _description %{expand:
Faster way to navigate your filesystem.}

%description %{_description}

%prep
%autosetup -n zoxide-%{version} -p1

%build
%cargo_build

%install
cargo install --root=%{buildroot}%{_prefix} --path=.

install -p -D -m0644 zoxide.plugin.zsh \
    %{buildroot}%{_datadir}/zsh/site-functions/zoxide.plugin.zsh
install -p -D -m0644 init.fish \
    %{buildroot}%{_datadir}/fish/vendor_completions.d/zoxide.fish
rm -f %{buildroot}%{_prefix}/.crates.toml \
      %{buildroot}%{_prefix}/.crates2.json

%files
%license LICENSE
%doc README.md CHANGELOG.md
%{_bindir}/zoxide
%{_datadir}/fish/vendor_completions.d/zoxide.fish
%{_datadir}/zsh/site-functions/zoxide.plugin.zsh

%changelog
* Mon May 10 2021 Felix Wang <topazus@outlook.com> - 0.7.0
- build update to 0.7.0
