%global debug_package %{nil}

Name:           xplr
Version:        0.8.4
Release:        1%{?dist}
Summary:        Hackable, minimal, fast TUI file explorer

License:        MIT
URL:            https://github.com/sayanarijit/xplr
Source:         %{url}/archive/v%{version}.tar.gz

BuildRequires:  cargo
BuildRequires:  rust
BuildRequires:  rust-packaging

%description
A hackable, minimal, fast TUI file explorer.

%prep
%autosetup -n %{name}-%{version} -p1

%build
cargo build --release

%install
install -p -D -m755 target/release/xplr %{buildroot}%{_bindir}/xplr

install -p -D -m644 src/config.yml %{buildroot}%{_datadir}/xplr/example
install -p -D -m644 assets/desktop/xplr.desktop %{buildroot}%{_datadir}/applications
for i in 128 16 32 64; do
    install -p -D -m644 assets/icon/xplr${i}.png \
        %{buildroot}%{_datadir}/icons/hicolor/${i}x${i}/apps/xplr.png
done
install -p -D -m644 assets/icon/xplr.svg \
    %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/xplr.svg

%files
%license LICENSE
%doc README.md
%{_bindir}/xplr

%changelog
* Mon May 10 2021 Felix Wang <topazus@outlook.com> - 0.8.4
- build update to 0.8.4
