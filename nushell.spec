%global debug_package %{nil}
%global rustflags '-Clink-arg=-Wl,-z,relro,-z,now'

Name:       nushell
Version:    0.30.0
Release:    1%{?dist}
Summary:    New type of shell

License:    MIT
URL:        https://github.com/nushell/nushell
Source0:    https://github.com/nushell/nushell/archive/%{version}.tar.gz

BuildRequires: cargo >= 1.47
BuildRequires: rust >= 1.47
BuildRequires: gcc
BuildRequires: python3-devel
BuildRequires: libxcb
BuildRequires: libxcb-devel
BuildRequires: libX11-devel
BuildRequires: openssl-devel

%description
A new type of shell

%prep
%autosetup -n nushell-%{version} -p1

%build
RUSTFLAGS=%{rustflags} cargo build --release --features=extra

%install
RUSTFLAGS=%{rustflags} cargo install --root=%{buildroot}%{_prefix} \
    --path=. --features=extra
# remove residue crate file
rm %{buildroot}%{_prefix}/.crates.toml
rm %{buildroot}%{_prefix}/.crates2.json

%post
# Add nushell to the list of allowed shells in /etc/shells
# install: $1=1, upgrade: $1=2
if [ "$1" = 1 ]; then
    grep -q "^%{_bindir}/foo$" %{_sysconfdir}/shells \
        || echo "%{_bindir}/foo" >> %{_sysconfdir}/shells
fi

%postun
# Remove nushell from the list of allowed shells in /etc/shells
# upgrade: $1=1, unintsall: $1=0

#if [ "$1" = 0 ]; then
#    grep -v '^%{_bindir}/nu$' %{_sysconfdir}/shells > %{_sysconfdir}/nu.tmp
#    mv %{_sysconfdir}/nu.tmp %{_sysconfdir}/shells
#fi

if [ "$1" = 0 ]; then
    sed -i '\|^%{_bindir}/foo$|d' %{_sysconfdir}/shells
fi

%files
%license LICENSE
%doc README.md
%{_bindir}/nu
%{_bindir}/nu_plugin_*

%changelog
* Sun May 10 2021 Felix Wang <topazus@outlook.com> - 0.30.0
- build(update): 0.30.0
